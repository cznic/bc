module modernc.org/bc

require (
	github.com/edsrzf/mmap-go v0.0.0-20170320065105-0bce6a688712 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20170806203942-52369c62f446 // indirect
	golang.org/x/crypto v0.0.0-20181106171534-e4dc69e5b2fd // indirect
	golang.org/x/sys v0.0.0-20181107165924-66b7b1311ac8 // indirect
	modernc.org/ccir v0.0.0-20181106174718-5753a3f77739 // indirect
	modernc.org/golex v1.0.0 // indirect
	modernc.org/httpfs v1.0.0
	modernc.org/internal v1.0.0 // indirect
	modernc.org/ir v1.0.0 // indirect
	modernc.org/mathutil v1.0.0 // indirect
	modernc.org/memory v1.0.0 // indirect
	modernc.org/strutil v1.0.0 // indirect
	modernc.org/virtual v1.0.0
	modernc.org/xc v1.0.0 // indirect
)
